import aiomysql
import pymysql
import asyncio
from asyncio.events import AbstractEventLoop
from aiomysql.connection import Connection


class MySQLError(Exception):
    """
    
    For errors
    
    """


class MySQL:

    connect: Connection

    def __init__(self, host: str, user: str, password: str, db: str, loop: AbstractEventLoop):
        self.host = host
        self.user = user
        self.password = password
        self.db = db
        self._data = None
        self.loop = loop or asyncio.get_event_loop()

    async def _connection(self):
        pool = await aiomysql.connect(host=self.host, user=self.user, password=self.password, db=self.db,
                                      loop=self.loop, autocommit=True)

        return pool

    def fetchone(self) -> dict | None:
        return self.cursor.fetchone()

    def fetchall(self) -> list | None:
        return self.cursor.fetchall()

    def fetchany(self, size: int = 10) -> list | None:
        return self.cursor.fetchany(size)

    async def cursor(self, query: str):
        async with self.connect.cursor(aiomysql.DictCursor) as self._cursor:
            try:
                await self._cursor.execute(query)
            except pymysql.err.ProgrammingError as e:
                raise MySQLError(e)
            except Exception as e:
                raise MySQLError(e)

        return self

    async def query(self, request: str):
        connect: Connection = await self._connection()
        try:
            await self.cursor(request)
        finally:
            await connect.commit()
            connect.close()
            return self



