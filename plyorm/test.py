from plyorm.db import Model
from plyorm.fields.text_field import TextField


class Person(Model):
    name = TextField(name="Name", default=False)


var = Person()
print(var.__class__.__dict__)
print(var.test())
